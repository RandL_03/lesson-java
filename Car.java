public abstract class Car {

    private final String model;
    /*
    Количество пассажиров
     */
    private int passengers;
    private int maxSpeed;

    private int speed;

    public Car(String model){
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getSpeed() {
        return speed;
    }

    public abstract void Stop();

    protected void Move()
    {
        System.out.println("Машина едет");
    }

    @Override
    public String toString() {
        return "Модель: "+getModel()+"; Текущая скорость: "+getSpeed()+";";
    }

    /*
    Ускорение до max скорости
     */
    public void boost(int sped)
    {
        speed += 10;
        if(speed<maxSpeed) {
            boost(speed);
        }
    }
}
