public class MyFirst {
    public static void main(String[] args) {
        Truck truck = new Truck("Камаз");
        System.out.println("Автомобиль: "+truck.toString());
        truck.boost(0);
        System.out.println("Автомобиль: "+truck.toString());
        truck.Stop();

        TrucMixer trucMixer = new TrucMixer("Камаз");
        trucMixer.boost(0);
        System.out.println("Автомобиль: "+trucMixer.toString());
        trucMixer.Stop();
    }
}
