public class Passenger extends Car{
    public Passenger(String model) {
        super(model);
        setPassengers(3);
        setMaxSpeed(100);
    }

    public Passenger(String model, int passenger){
        super(model);
        setPassengers(passenger);
    }

    @Override
    public void Stop() {
        System.out.println("Машина остановилась за 1 с.");
    }

}
