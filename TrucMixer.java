public class TrucMixer extends Car implements ConcreteMixer{
    public TrucMixer(String model) {
        super(model);
    }

    @Override
    public void Stop() {
        System.out.println("Машина остановилась за 7c");
    }


    @Override
    public void Mix() {
            System.out.println("Мешает бетон");
    }
}
