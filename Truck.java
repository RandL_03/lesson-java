public class Truck extends Car {

    public Truck(String model){
        super(model);
        setPassengers(3);
        setMaxSpeed(100);
    }

    @Override
    public void Stop() {
            System.out.println("Машина остановилась за 5c");
    }

    public void Traffic(int mass) {
        System.out.println("Перевозка товаров "+mass+" т.");
    }

    public void Traffic(double mass){
        System.out.println("Перевозка товаров "+mass+" т.");
    }


}
